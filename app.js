var mongojs = require("mongojs");
var db = mongojs("localhost:27017/ThePixelWorld", ["account","progress"]);

var express = require("express");
var app = express();

var serv = require("http").Server(app);

app.get('/', function(req,res) {
    res.sendFile(__dirname + '/client/index.html');
});

app.use('/client', express.static(__dirname + '/client'));
app.use('/favicon.ico', express.static(__dirname + '/client/favicon.ico'));

serv.listen(2000);
console.log("Server started.");
var SOCKET_LIST = {};
const servermsg = "Server: ";

const Commands = require('./server/Commands')
const Utils = require('./server/Utils')
const Entity = require('./server/Entities/Entity')
const Player = require('./server/Entities/Player')
const Bullet = require('./server/Entities/Bullet')


var isValidPassword = function(data, cb){
    db.account.find({username:data.username, password:data.password},function(err,res){
        if(res.length > 0)
            cb(true);
        else
            cb(false);
    });
}

var isUsernameTaken = function(data, cb){
    db.account.find({username:data.username},function(err,res){
        if(res.length > 0)
            cb(true);
        else
            cb(false);
    });
}

var addUser = function(data, cb){
    db.account.insert({username:data.username, password:data.password},function(err){
        cb();
    });
}

var io = require('socket.io')(serv, {});
io.sockets.on('connection', function(socket){
    socket.id = Math.random();
    SOCKET_LIST[socket.id] = socket;

    socket.on('signIn', function(data){
        isValidPassword(data, function(res){
            if (res){
                Player.Player.onConnect(socket, data.username);
                socket.playerName = data.username;
                socket.emit('signInResponse', {success:true});
            } else {
                socket.emit('signInResponse', {success:false});
            }
        });
    });
    socket.on('signUp', function(data){
        isUsernameTaken(data, function(res){
            if (res){
                socket.emit('signUpResponse', {success:false});
            } else {
                addUser(data, function(){
                    socket.emit('signUpResponse', {success:true});
                });
            }
        });
    });

    socket.on('disconnect', function(){
        delete SOCKET_LIST[socket.id];
        Player.Player.onDisconnect(socket);
    });
    socket.on('sendMsgToServer', function(data){
        if (data)
            for (var i in SOCKET_LIST){
                SOCKET_LIST[i].emit('addToChat', {ctimestamp: Utils.getTimeStamp(0), content: socket.playerName + ': ' + Utils.escapeHTML(Utils.chatEmote(data)), tab: "world"});
            }
    });

    socket.on('command', function(data){
        Commands.dispatch(data, socket, SOCKET_LIST, servermsg, db, Player);
    });

    socket.on('evalServer', function(data){
        Utils.hasPermission(db, socket.playerName, 80, function(res){
            if (res)
                socket.emit('evalAnswer', eval(data));
            else
                socket.emit('addToChat', {ctimestamp: getTimeStamp(0), content: servermsg + "you dont have permission to use this command!", tab: "any"});
        });
    });
});

// erm... the game loop
setInterval(function(){
    var pack = {
        player: Player.Player.update(),
        bullet: Bullet.Bullet.update()
    }
    
    for(var i in SOCKET_LIST){
        var socket = SOCKET_LIST[i];
        socket.emit('init', Entity.initPack);
        socket.emit('update', pack);
        socket.emit('remove', Entity.removePack);
    }
    Entity.initPack.player = [];
    Entity.initPack.bullet = [];
    Entity.removePack.player = [];
    Entity.removePack.bullet = [];

}, 1000/25);
