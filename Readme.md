## ThePixelWorld

### About
**ThePixelWorld** is an inspiration of [TheManaWorld](https://www.themanaworld.org)\
**License**: all files (if not noted otherwise) in this repository are GPLv3 licensed.\
**Author**: jak1 (Mike Wollmann) \<jak1 themanaworld org\>

### Used Languages
>   php (server config)\
    html5 (basic canvas)\
    css (ui / divs / ...)\
    js, node-js (game back & frontend)\
    bash / sh (help scripts)
    
### Tools:
>   Mapeditor: [Tiled](https://www.mapeditor.org/)\
    PixelArt: [KRITA](https://krita.org), [GIMP](https://www.gimp.org/)\
    Coding: [Visual Studio Code](https://code.visualstudio.com/)

### [FAQ](docs/FAQ.md)