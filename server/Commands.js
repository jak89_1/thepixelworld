const Utils = require("./Utils");

var dispatch = function (data, socket, SOCKET_LIST, servermsg, db, Player)
{
    var modifyer = data[0];
    var command = data.slice(1).split(' ');

    //staff cmd
    if (modifyer === '@')
    {
        Utils.hasPermission(db, socket.playerName, 1, function (res)
        {
            if (res)
                switch (command[0].toLowerCase())
                {
                    // @bc (args: <message>)
                    case "bc":
                    case "broadcast":
                        res = Utils.joinArgs(command, 1, 0);
                        for (var i in SOCKET_LIST)
                        {
                            SOCKET_LIST[i].emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: "<font color='red' weight='bold'>GlobalAnnounce: " + Utils.escapeHTML(res) + "</font>", tab: "world" });
                        }
                        break;

                    // @lbc (args: <message>)
                    case "lbc":
                    case "localbroadcast":
                        res = Utils.joinArgs(command, 1, 0);
                        for (var i in Player.Player.list)
                        {
                            if (Player.Player.list[i].map == Player.Player.list[socket.id].map)
                                SOCKET_LIST[i].emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: "<font color='red' weight='bold'>GlobalAnnounce: " + Utils.escapeHTML(res) + "</font>", tab: "world" });
                        }
                        break;

                    // @who (args: none)
                    case "who":
                        res = servermsg;
                        for (var i in Player.Player.list)
                        {
                            res += Player.Player.list[i].playerName + ", ";
                        }
                        socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: res, tab: "any" });
                        break;

                    // @where (args:[playername])
                    case "where":
                        var found = false;
                        if (command.length <= 1)
                            command[1] = socket.playerName;
                        for (var i in Player.Player.list)
                        {
                            if (Player.Player.list[i].playerName.includes(command[1]))
                            {
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + Player.Player.list[i].playerName + ": " + Player.Player.list[i].map + " [" + Player.Player.list[i].x + ", " + Player.Player.list[i].y + "]", tab: "any" });
                                found = true;
                            }
                        }
                        if (!found)
                            socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Player not found", tab: "any" });
                        break;

                    // @goto (args:<playername>)
                    case "goto":
                        var found = false;
                        if (command.length <= 1)
                            command[1] = socket.playerName;
                        for (var i in Player.Player.list)
                        {
                            if (Player.Player.list[i].playerName.includes(command[1]))
                            {
                                Player.Player.list[socket.id].map = Player.Player.list[i].map;
                                Player.Player.list[socket.id].x = Player.Player.list[i].x;
                                Player.Player.list[socket.id].y = Player.Player.list[i].y;
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Warped to " + Player.Player.list[i].playerName, tab: "any" });
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Player not found", tab: "any" });
                        break;

                    // @recall (args:<playername>)
                    case "recall":
                        var found = false;
                        if (command.length <= 1)
                            command[1] = socket.playerName;
                        for (var i in Player.Player.list)
                        {
                            if (Player.Player.list[i].playerName.includes(command[1]))
                            {
                                Player.Player.list[i].map = Player.Player.list[socket.id].map;
                                Player.Player.list[i].x = Player.Player.list[socket.id].x;
                                Player.Player.list[i].y = Player.Player.list[socket.id].y;
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Recalled " + Player.Player.list[i].playerName + " to you.", tab: "any" });
                                SOCKET_LIST[Player.Player.list[i].id].emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "You were Recalled from " + socket.playerName, tab: "any" });
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Player not found", tab: "any" });
                        break;

                    // @warp/@tele/@teleport (args:[map] [<x> <y>])
                    case "warp":
                    case "tele":
                    case "teleport":
                        Utils.hasPermission(db, socket.playerName, 60, function (res)
                        {
                            if (res)
                            {
                                if (command.length < 2 || command.length > 4)
                                {
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + Utils.escapeHTML("Wrong syntax: @warp [map] [<x> <y>]"), tab: "any" });
                                    return;
                                }
                                else if (command.length == 2)
                                {
                                    Player.Player.list[socket.id].map = command[1];
                                    var new_x = Math.random() * 500; // TODO: map size?!
                                    var new_y = Math.random() * 500;
                                    Player.Player.list[socket.id].x = new_x;
                                    Player.Player.list[socket.id].y = new_y;
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Warped to map:" + command[1] + " x:" + new_x + " y:" + new_y, tab: "any" });
                                }
                                else if (command.length == 3)
                                { // only cords
                                    // TODO: check if map exists
                                    Player.Player.list[socket.id].x = parseInt(command[1]);
                                    Player.Player.list[socket.id].y = parseInt(command[2]);
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Warped to x:" + command[1] + " y:" + command[2], tab: "any" });
                                }
                                else if (command.length == 4)
                                { // map x y
                                    // TODO: check if map exists
                                    Player.Player.list[socket.id].map = command[1];
                                    Player.Player.list[socket.id].x = parseInt(command[2]);
                                    Player.Player.list[socket.id].y = parseInt(command[3]);
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Warped to map:" + command[1] + " x:" + command[2] + " y:" + command[3], tab: "any" });
                                }
                            }
                            else
                            {
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "you dont have permission to use this command!", tab: "any" });
                            }
                        });
                        break;

                    // @hide
                    case "hide":
                        Utils.hasPermission(db, socket.playerName, 60, function (res)
                        {
                            if (res)
                            {
                                Player.Player.list[socket.id].ignorePlayerAttack = !Player.Player.list[socket.id].ignorePlayerAttack;
                                if (Player.Player.list[socket.id].ignorePlayerAttack)
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "You are now immune to player and monster attacks", tab: "any" });
                                else
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "You are no longer immune to player and monster attacks", tab: "any" });
                            }
                            else
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "you dont have permission to use this command!", tab: "any" });
                        });
                        break;

                    // @setgm (args:<playername> <lvl>)
                    case "setgm":
                        Utils.hasPermission(db, socket.playerName, 99, function (res)
                        {
                            if (res)
                                updateGMLevel(db, command[1], parseInt(command[2]), function ()
                                {
                                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "GM level set.", tab: "any" });
                                });
                            else
                                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "you dont have permission to use this command!", tab: "any" });
                        });
                        break;
                    default:
                        socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Command not found.", tab: "any" });
                        break;
                }
            else
            {
                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "you dont have permission to use this command!", tab: "any" });
            }
        });
    }

    // player cmd
    else if (modifyer === '/')
    {
        switch (command[0])
        {
            // /who (args: none)
            case "who":
                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + Object.keys(Player.Player.list).length + " Player" + ((Object.keys(Player.Player.list).length > 1) ? "s" : "") + " Online.", tab: "any" });
                break;

            // /where (args: none)
            case "where":
                socket.emit('addToChat', {
                    ctimestamp: Utils.getTimeStamp(0), content: servermsg + Player.Player.list[socket.id].playerName + ": " +
                        Player.Player.list[socket.id].map + " [" + Player.Player.list[socket.id].x + ", " + Player.Player.list[socket.id].y + "]", tab: "any"
                });

                break;

            // /whisper (args: <playername> <message>)
            case "w":
            case "whisper":
            case "tell":
            case "msg":
                var found = false;

                for (var i in Player.Player.list)
                {
                    if (Player.Player.list[i].playerName == command[1])
                    {
                        var res = Utils.escapeHTML(Utils.joinArgs(command, 2, 0));
                        socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: socket.playerName + ": " + res, tab: Player.Player.list[i].playerName });
                        SOCKET_LIST[Player.Player.list[i].id].emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: socket.playerName + ": " + res, tab: socket.playerName });
                        found = true;
                        break;
                    }
                }
                if (!found)
                    socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Player not found", tab: "any" });
                break;

            default:
                socket.emit('addToChat', { ctimestamp: Utils.getTimeStamp(0), content: servermsg + "Command not found.", tab: "any" });
                break;
        }
    }

    // guild commands
    else if (modifyer === '#')
    {

    }
    // party commands
    else if (modifyer === '.')
    {

    }
    // help commands
    else if (modifyer === '?')
    {
        // ?
    }
    else
    {
        // this should never happen...
    }
}


var updateGMLevel = function (db, username, lvl, cb)
{
    db.account.update({ username: username }, { $set: { gmlvl: lvl } }, function (err)
    {
        cb();
    });
}

module.exports = { dispatch };
