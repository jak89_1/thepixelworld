//removes (number)remArgsBefore and (number)remArgsAfter from argument array/list
var joinArgs = function (argsList, remArgsBefore, remArgsAfter)
{
    var res = "";
    for (var i = 0; i < argsList.length; i++)
    {
        if (i < remArgsBefore)
            continue;
        if (argsList.length - i <= remArgsAfter)
            continue;
        res += argsList[i] + " ";
    }
    return res;
}

// gets the current (weird formated) timestamp
var getTimeStamp = function ()
{
    var date_ob = new Date();
    var day = addLeadingZero(date_ob.getDate());
    var month = addLeadingZero(date_ob.getMonth());
    var year = date_ob.getFullYear();
    var hours = addLeadingZero(date_ob.getHours());
    var minutes = addLeadingZero(date_ob.getMinutes());
    var seconds = addLeadingZero(date_ob.getSeconds());

    return (year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds);
}

// formats number (n) to 2-digit
var addLeadingZero = function (n)
{
    if (n <= 9)
        return "0" + n;
    return n;
}

var escapeHTML = function (unsafe)
{
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

var chatEmote = function (msg)
{
    return msg.replace(":D", "😀")
        .replace("O:)", "😇")
        .replace("0:)", "😇")
        .replace(":)", "🙂")
        .replace(":(", "🙁")
        .replace(":o", "😮")
        .replace(":O", "😲")
        .replace(":'(", "😢")
        .replace(":')", "🥲")
        .replace(":P", "😛");
}

var hasPermission = function (db, username, req, cb)
{
    db.account.find({ username: username }, function (err, res)
    {
        if (res[0].gmlvl >= req)
            cb(true);
        else
            cb(false);
    });
}


module.exports = { joinArgs, getTimeStamp, addLeadingZero, escapeHTML, chatEmote, hasPermission };
