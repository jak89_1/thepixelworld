
var initPack = { player: [], bullet: [] };
var removePack = { player: [], bullet: [] };

var Entity = function ()
{
    var self = {
        x: 250,
        y: 250,
        map: '002-1',
        speedX: 0,
        speedY: 0,
        id: "",
    }
    self.update = function ()
    {
        self.updatePosition();
    }
    self.updatePosition = function ()
    {
        self.x += self.speedX;
        self.y += self.speedY;
    }
    self.getDistance = function (pt)
    {
        return Math.sqrt(Math.pow(self.x - pt.x, 2)) + Math.pow(self.y - pt.y, 2);
    }
    return self;
}

module.exports = { Entity, initPack, removePack };
