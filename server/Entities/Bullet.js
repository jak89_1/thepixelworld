const Entity = require('./Entity');
const Player = require('./Player');

var Bullet = function (parent, angle)
{
    var self = Entity.Entity();
    self.id = Math.random();
    self.speedX = Math.cos(angle / 180 * Math.PI) * 10;
    self.speedY = Math.sin(angle / 180 * Math.PI) * 10;
    self.dir = 0;
    self.parent = parent;
    self.timer = 0;
    self.toRemove = false;

    var super_update = self.update;
    self.update = function ()
    {
        if (self.timer++ > 100)
            self.toRemove = true;
        super_update();
        for (var i in Player.list)
        {
            var p = Player.list[i];
            if (self.getDistance(p) < 32 && self.parent !== p.id)
            {
                if (!p.ignorePlayerAttack)
                    p.hp -= 1;
                if (p.hp <= 0)
                {
                    var shooter = Player.list[self.parent];
                    if (shooter)
                        shooter.score += 1;
                    p.hp = p.hpMax;
                    p.x = Math.random() * 500;
                    p.y = Math.random() * 500;

                }
                self.toRemove = true;
            }
        }
    }

    self.getInitPack = function ()
    {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
            map: self.map,
            dir: self.dir,
        };
    }

    self.getUpdatePack = function ()
    {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
            map: self.map,
            dir: self.dir,
        };
    }

    Bullet.list[self.id] = self;
    Entity.initPack.bullet.push(self.getInitPack());
    return self;
}

Bullet.list = {};

Bullet.update = function ()
{
    var pack = [];
    for (var i in Bullet.list)
    {
        var bullet = Bullet.list[i];
        bullet.update();
        if (bullet.toRemove)
        {
            delete Bullet.list[i];
            Entity.removePack.bullet.push(bullet.id);
        }
        else
            pack.push(bullet.getUpdatePack());
    }
    return pack;
}

Bullet.getAllInitPack = function ()
{
    var bullets = [];
    for (var i in Bullet.list)
        bullets.push(Bullet.list[i].getInitPack());
    return bullets;
}

module.exports = { Bullet };
exports.list = Bullet.list;
