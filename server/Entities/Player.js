var consts = require('../../client/consts')
const Entity = require('./Entity');
const Bullet = require('./Bullet');


var Player = function (id)
{
    var self = Entity.Entity();
    self.id = id;
    self.playerName = "";
    self.pressingRight = false;
    self.pressingLeft = false;
    self.pressingUp = false;
    self.pressingDown = false;
    self.pressingAttack = false;
    self.mouseAngle = 0;
    self.maxSpeed = 10;
    self.hp = 10;
    self.dir = 0;
    self.hpMax = 10;
    self.score = 0;
    self.move = true;
    self.sit = false;
    self.inInventory = false;
    self.ignorePlayerAttack = false;
    self.isMoving = false;

    var super_update = self.update;
    self.update = function ()
    {
        self.updateSpeed();
        super_update();
        if (self.pressingAttack)
        {
            self.shootBullet(self.mouseAngle);
        }
    }

    self.shootBullet = function (angle)
    {
        var b = Bullet.Bullet(self.id, angle);
        b.x = self.x
        b.y = self.y
    }

    self.updateSpeed = function ()
    {
        if (self.pressingRight)
        {
            self.speedX = self.maxSpeed * self.move;
            self.dir = 3;
        }
        else if (self.pressingLeft)
        {
            self.speedX = -self.maxSpeed * self.move;
            self.dir = 1;
        }
        else
            self.speedX = 0;

        if (self.pressingUp)
        {
            self.speedY = -self.maxSpeed * self.move;
            self.dir = 2;
        }
        else if (self.pressingDown)
        {
            self.speedY = self.maxSpeed * self.move;
            self.dir = 0;
        }
        else
            self.speedY = 0;
        // dont sit while moving, in case we dont press shift(rotation/dir)
        if (self.pressingAttack + self.pressingDown + self.pressingLeft + self.pressingRight + self.pressingUp != 0 && self.move)
        {
            self.sit = false;
            self.isMoving = true;
        }
    }

    self.getInitPack = function ()
    {
        return {
            id: self.id,
            playerName: self.playerName,
            x: self.x,
            y: self.y,
            map: self.map,
            hp: self.hp,
            hpMax: self.hpMax,
            score: self.score,
            dir: self.dir,
            sit: self.sit,
            ignorePlayerAttack: self.ignorePlayerAttack,
        };
    }
    self.getUpdatePack = function ()
    {
        return {
            id: self.id,
            playerName: self.playerName,
            x: self.x,
            y: self.y,
            map: self.map,
            score: self.score,
            hp: self.hp,
            dir: self.dir,
            sit: self.sit,
            inInventory: self.inInventory,
            ignorePlayerAttack: self.ignorePlayerAttack,
            isMoving:self.isMoving,
        };
    }
    Player.list[id] = self;
    Entity.initPack.player.push(self.getInitPack());
    return self;
}

Player.list = {};

Player.onConnect = function (socket, pName)
{
    var player = Player(socket.id);

    player.playerName = pName;

    socket.on('keyPress', function (data)
    {
        if (data.shift)
            player.move = false;
        else
            player.move = true;

        if (data.inputId === 'left')
        {
            player.pressingLeft = data.state;
            player.dir = 1;
            player.isMoving = data.state;
        }
        else if (data.inputId === 'right')
        {
            player.pressingRight = data.state;
            player.dir = 3;
            player.isMoving = data.state;
        }
        else if (data.inputId === 'up')
        {
            player.pressingUp = data.state;
            player.dir = 2;
            player.isMoving = data.state;
        }
        else if (data.inputId === 'down')
        {
            player.pressingDown = data.state;
            player.dir = 0;
            player.isMoving = data.state;
        }
        else if (data.inputId === 'sit')
        {
            player.sit = data.state;
            player.isMoving = false;
        }
        else if (data.inputId === 'attack')
        {
            player.pressingAttack = data.state;
        }
        else if (data.inputId === 'mouseAngle')
        {
            player.mouseAngle = data.state;
        }
        if (data.inputId === 'inInventory')
        {
            player.inInventory = data.state;
        }
    });

    socket.emit('init', {
        selfId: socket.id,
        player: Player.getAllInitPack(),
        bullet: Bullet.Bullet.getAllInitPack(),
    });
}

Player.getAllInitPack = function ()
{
    var players = [];
    for (var i in Player.list)
        players.push(Player.list[i].getInitPack());
    return players;
}

Player.onDisconnect = function (socket)
{
    delete Player.list[socket.id];
    Entity.removePack.player.push(socket.id);
}

Player.update = function ()
{
    var pack = [];
    for (var i in Player.list)
    {
        var player = Player.list[i];
        player.update();
        pack.push(player.getUpdatePack());
    }
    return pack;


}

module.exports = { Player };
exports.list = Player.list;
