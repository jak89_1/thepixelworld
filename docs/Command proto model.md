commands:

    user ( / ):
        player (default):
            /who
            /help
            /afk
            /back
            /where
            /yes /no
            /version
            /git
            /gm [message]
            /exit
            /setmail

        helper ((requestable) adds a ?):
            /help <username> <helppage> (opens the help page for the given player)

        supporter ((buyable (once 9.99)) adds a lightblue vip):
            /ninja (makes the player invisible for 1 min. (usable once every 20min))

        supporter+ ((buyable (9.99/months)) adds a darkblue vip):
            /ninja (makes the player invisible for 1 min. (usable once every 10min))
            /tpa <username> (requires /yes from the player)
            

    guild ( # ):
        #motd <msg>
        #create <guildname> (quest/npc only?)
        #exp <1/0,on/off,enable/disable>
        #items <1/0,on/off,enable/disable>
        #hall (goto guildhall)
        #gvg <1/0,on/off,enable/disable>
        #settings
        #invite <playername>

    party ( . ):
        .create <partyname>
        .exp [1/0,on/off,enable/disable]
        .items [1/0,on/off,enable/disable]
        .settings

    staff ( @ ):
        gm (adds GM):
            @where [username]
            @who
            @whomap [mapname]
            @goto <username>
            @recall <username>
            @linus/@hugo/@back
            @[un]block <username> [reason]
            @[un]ban <username> [<time> [reason]]
            @kick <username> [reason]
            @spawn <mob-name/id> [quantity] [name] [stats] [callfunc]
            @summon <mob-name/id> [quantity] [name] [stats] [callfunc]
            @[set]home [id]
            @[in]visible
            @usergroup <username> [player/helper/supporter/supporter+]
            @stfu [time (if not set infinite, or till the gm leaves the map)]
            @mute <playername> <time>
            @npc <npcname>
            @exp <100-250> (rate in %)

        dev (adds wrench):
            @debug <param[s]>
            @weather <rain/fog/snow/sun/clear>
            @season <spring/summer/autumn/winter>
            @time <0100/day(08:00)/night(20:00)>
            @[un]hide
            @bc <message>
            @lbc <message>
            @l <message>
            @t <message>
            @disablenpc <npcname>
            @enablenpc <npcname>
            @getguildid <guildname>
            @getpartyid <partyname>

        admin (adds red star):
            @lvl <+-value>
            @kickall
            @restart [time]
            @shutdown [time]
            @recallall
            @recallparty <partyname>
            @recallpleader <partyname>
            @recallguild <guildyname>
            @recallgleader <guildyname>
            @setstate <queststate> <value> [playername]
            @getstate <queststate> [playername]
            @item <itemID/Name> [quantity] [stats] [callfunc]
            @save
