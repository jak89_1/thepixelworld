Database:

    Entity(
        ID,
        Name,
        pos(x, y, z-order, map, sit(0/1)),
        equipment([slot, itemID]),
        flags([muted, banned, blocked, canMove]),
    );

    DB:Monster(
        Entity(),
        behavior(active, passive, shy),
        exp(),
        items([]),
        questflags(),
    );

    DB:players(
        Entity,
        username,
        password,
        email,
        group(usergroup, staffgroup), 
        inventory([itemID, quantity]),
        storage([itemID, quantity]),
        money(char, bank)
        states(questID or Name, value),
        homes([homeID,[x,y,map]])
        guild(guildID),
        party(partyID),
        ...
    );

    DB:guilds(
        guildID,
        guildname,
        storage([itemID, quantity]),
        money(bank),
        player([id, grade, userflags]),
        states(guildQuestID or Name, value),
        guildflags(exp, item),
    );

    DB:partys(
        partyID,
        partyname,
        player([id, grade]),
        partyflags(exp, item),
    );

